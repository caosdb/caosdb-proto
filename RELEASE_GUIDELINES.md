# Release Guidelines for the CaosDB gRPC API

This document specifies release guidelines in addition to the generel release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* CHANGELOG.md is up-to-date.
* DEPENDENCIES.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Update CHANGELOG.md

3. Check all general prerequisites.

4. Prepare [CMakeLists.txt](./CMakeLists.txt): Check the `CAOSDB_PROTO_VERSION`
   version variable.

5. Merge the release branch into the main branch.

6. Tag the latest commit of the main branch with `v<VERSION>`.

7. Delete the release branch.

8. Prepare the next release cycle by updating
   [CMakeLists.txt](./CMakeLists.txt) - bump the version number of
   `CAOSDB_PROTO_VERSION` to the next minor number.

9. Create release on gitlab.indiscale.com and gitlab.com
