# GENERAL

No dependencies.

# BUILD DOCS

* >=cmake-3.13
* >=protoc-3.12
* =[protoc-gen-doc](https://github.com/pseudomuto/protoc-gen-doc@)-1.4.1
* >=python-3.7
* >=pip-21.0.1
* python packages from the `doc/requirements.txt` file.

# LINTING

* Dependencies from BUILD DOCS
* >=buf-0.43.2 (install via pip)

# FORMATTING

* >=clang-formatting-11
