ALL:
	@echo "THIS IS ONLY FOR DEVELOPMENT. DON'T USE IT"

formatting:
	clang-format -i $$(find proto/ -type f -iname "*.proto")

linting:
	buf lint
