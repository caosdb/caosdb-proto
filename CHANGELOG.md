# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.3] - 2023-01-19
(Florian Spreckelsen)

### Added

* Messages for the Result of `SELECT` queries.

## [0.2] - 2022-03-25
(Timm Fitschen)

### Added

* EXPERIMENTAL Access Control Management API (caosdb.acm.v1alpha1)
* EntityACL API as extension of the Entity API (caosdb.entity.v1) for retrieval and update of entity acls.
* SessionInfo API as extension of the Info API (caosdb.info.v1) for the
  retrival of information about the current users session.

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.1] 2021-11-08
(Timm Fitschen)

The initial release contains two separate GRPC APIs, one for transactions of
entities (Entity API) and one for general information about the server.

Note that the authentication of clients (and/or servers) is not defined as
a protobuf API. Instead, the server should provide authentication schemes based
on the transport protocols (usually HTTP/2) and indicate the options in its own
documentation.

### Added

#### Entity API (v1)

The Entity API defines entities, the first-class citizens of CaosDB and the
transactions for insertion, updates, retrieval and deletion of entities and the
upload and download of binary files.

Entities can be retrieved by id or by query. Several entities can be processed
in a single transaction and the transaction types (insertion, update, deletion,
retrieval) can be mixed in a single transaction.

Notable current limitations of the API:

 * The error handling is still not perfect. Properties cannot have messages
   right now and only a few error messages are defined in the API.
 * The version message can only represent the version id of an entity. The
   history cannot be retrieved.
 * The entity's access control lists cannot be changed or retrieved.
 * The entity state cannot be changed or retrieved.
 * A lot of flags (e.g. inheritance among other things) which are implemented
   in the REST API of CaosDB are not supported.

The current release is designed in a way that these limitation can be be
overcome in future releases of the API without breaking backwards compatibility.

#### Info API (v1)

The Info API is strict read-only API which provides useful non-confidential
information about the server.

The initial release of this API is quite limited. Only the server version can
be retrieved.
